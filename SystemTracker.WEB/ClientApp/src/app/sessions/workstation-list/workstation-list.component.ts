import { Component, OnInit, OnDestroy } from '@angular/core';
import { WorkstationService } from '../../services/workstation.service';
import { ConnectedWorkstationInfo } from '../../models/connected-workstations-info.model';
import { HubConnection, HubConnectionBuilder, LogLevel, HttpTransportType } from '@aspnet/signalr';
import { environment } from '../../../environments/environment';
import { SessionLogoffModel } from '../../models/session-logoff.model';

import "rxjs/add/operator/do";
import { Observable } from 'rxjs';

@Component({
    selector: 'workstation-list',
    templateUrl: './workstation-list.component.html',
})
export class WorkstationListComponent implements OnInit, OnDestroy {
    private hubConnection: HubConnection;

    refreshing: boolean;
    workstations: ConnectedWorkstationInfo[] = new Array<ConnectedWorkstationInfo>();

    constructor(private servier: WorkstationService) { }

    private initSignalR(): void {
        this.hubConnection = new HubConnectionBuilder()
            .configureLogging(LogLevel.Trace)
            .withUrl(`${environment.api_url}/sessionhub?group=web`)
            .build();

        this.hubConnection.on('ReceiveWorkstationInfo', (i) => this.OnUpdateInfo(i));
        this.hubConnection.on('RemoveUserSession', (i) => this.OnWorkstationDisconected(i));

        this.hubConnection.start();
    }

    private OnUpdateInfo(info: ConnectedWorkstationInfo) {
        let index = this.workstations.findIndex((i) => i.connectionId == info.connectionId);

        if(index < 0) this.workstations.push(info);
        else this.workstations.splice(index, 1, info);
    }

    private OnWorkstationDisconected(connectionId: string) {
        let index = this.workstations.findIndex((i) => i.connectionId == connectionId);

        if(index >= 0) this.workstations.splice(index, 1);
    }

    public OnSessionLogoff(logoffInfo: SessionLogoffModel){
        this.hubConnection.invoke('LogoffSessionOnWorkstation', logoffInfo)
    }

    ngOnInit(): void { 
        this.servier.GetConnectedWorkstations()
            .do(r => this.workstations = r)
            .do(r => this.initSignalR())
            .subscribe();
    }

    public RefreshData(): void {
        this.refreshing = true;

        this.hubConnection.stop()
            .then(() => 
                this.servier.GetConnectedWorkstations()
                    .do(r => this.workstations = r)
                    .do(r => Observable.fromPromise(this.hubConnection.start()))
                    .do(r => this.refreshing = false)
                    .subscribe()
            );
    }

    ngOnDestroy(): void {
        this.hubConnection.stop();
    }
}
