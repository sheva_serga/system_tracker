import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SessionLogoffModel } from '../../models/session-logoff.model';
import { ConnectedWorkstationInfo } from '../../models/connected-workstations-info.model';

@Component({
    selector: 'workstation-component',
    templateUrl: './workstation.component.html'
})
export class WorkstationComponent {
    @Input() workstation: ConnectedWorkstationInfo;
    @Output() onSessionLogoff: EventEmitter<SessionLogoffModel> = new EventEmitter<SessionLogoffModel>();
    
    constructor() { }

    onSessionRemove(sessionId: number) {
        this.onSessionLogoff.emit( { connectionId: this.workstation.connectionId, sessionId } as SessionLogoffModel)
    }
}
