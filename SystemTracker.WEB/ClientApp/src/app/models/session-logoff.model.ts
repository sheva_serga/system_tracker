export class SessionLogoffModel {
    connectionId: string;
    sessionId: number;
}