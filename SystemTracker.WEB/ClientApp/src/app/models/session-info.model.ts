export class SessionInfo {
    sessionId: number; 
    domain: string; 
    userName: string; 
    state: string;
}