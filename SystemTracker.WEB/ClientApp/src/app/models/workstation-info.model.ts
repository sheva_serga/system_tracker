import { SessionInfo } from "./session-info.model";

export class WorkstationInfo {
    machineName: string; 
    sessions: SessionInfo[];
}