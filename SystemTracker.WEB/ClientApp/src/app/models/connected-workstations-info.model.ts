import { WorkstationInfo } from "./workstation-info.model";

export class ConnectedWorkstationInfo {
    connectionId: string; 
    info: WorkstationInfo;
}