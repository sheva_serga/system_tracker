import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { WorkstationListComponent } from './sessions/workstation-list/workstation-list.component';
import { WorkstationService } from './services/workstation.service';
import { WorkstationComponent } from './sessions/workstation/workstation.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    WorkstationListComponent, 
    WorkstationComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'workstations', component: WorkstationListComponent }
    ])
  ],
  providers: [WorkstationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
