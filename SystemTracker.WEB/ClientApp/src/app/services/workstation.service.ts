import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ConnectedWorkstationInfo } from '../models/connected-workstations-info.model';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class WorkstationService {

    private serviceUrl: string = `${environment.api_url}/api/workstations/all`;

    constructor(private http: HttpClient) { }

    public GetConnectedWorkstations(): Observable<ConnectedWorkstationInfo[]> {
        return this.http.get<ConnectedWorkstationInfo[]>(this.serviceUrl);
    }
}