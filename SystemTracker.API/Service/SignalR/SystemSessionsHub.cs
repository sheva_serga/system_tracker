﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystemTracker.API.Services.ClientStore;
using SystemTracker.Common.Models;
using SystemTracker.Common.Security;

namespace SystemTracker.API.Services.SignalR
{
    public interface ISystemTrackingHubClient
    {
        Task ReceiveWorkstationInfo(ConnectedWorkstationInfo info);
        Task RemoveUserSession(String connectionId);

        Task LogoffSession(Int32 sessionId);
    }

    public class SystemTrackingHub : Hub<ISystemTrackingHubClient>
    {
        private readonly IClientStoreService _storeService;
        private readonly ObjcetEncriptionService _encriptionService;

        public SystemTrackingHub(IClientStoreService storeService, ObjcetEncriptionService encriptionService)
        {
            _storeService = storeService;
            _encriptionService = encriptionService;
        }

        public async Task SendWorkstationInfo(EncryptedString encriptedInfo)
        {
            WorkstationInfo info = _encriptionService.Decript<WorkstationInfo>(encriptedInfo.Data);

            var connectedInfo = new ConnectedWorkstationInfo(Context.ConnectionId, info);

            _storeService.AddOrUpdate(connectedInfo);

            await Clients.Group("web").ReceiveWorkstationInfo(connectedInfo);
        }

        
        public async Task LogoffSessionOnWorkstation(SessionLogoffModel logoffModel)
        {
            await Clients.Client(logoffModel.ConnectionId).LogoffSession(logoffModel.SessionId);;
        }

        public override async Task OnConnectedAsync()
        {
            var httpContext = Context.GetHttpContext();

            await Groups.AddToGroupAsync(Context.ConnectionId, httpContext.Request.Query["group"]);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var httpContext = Context.GetHttpContext();
            
            if (httpContext.Request.Query["group"] == "workstations")
            {
                _storeService.Remove(Context.ConnectionId);

                await Clients.Group("web").RemoveUserSession(Context.ConnectionId);
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}
