﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using SystemTracker.Common.Models;

namespace SystemTracker.API.Services.ClientStore
{
    internal class InMemoryClientStoreService : IClientStoreService
    {
        private ConcurrentDictionary<String, ConnectedWorkstationInfo> _clients = new ConcurrentDictionary<String, ConnectedWorkstationInfo>();

        public InMemoryClientStoreService() { }

        public List<ConnectedWorkstationInfo> GetAllWorkstations() => _clients.Values.ToList();

        public void Remove(String connectionId) => _clients.TryRemove(connectionId, out ConnectedWorkstationInfo info);
        public void AddOrUpdate(ConnectedWorkstationInfo info) => _clients.AddOrUpdate(info.ConnectionId, info, (k, v) => info);  
    }
}
