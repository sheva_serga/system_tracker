﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystemTracker.Common.Models;

namespace SystemTracker.API.Services.ClientStore
{
    public interface IClientStoreService
    {
        void AddOrUpdate(ConnectedWorkstationInfo info);
        void Remove(String connectionId);

        List<ConnectedWorkstationInfo> GetAllWorkstations();
    }
}
