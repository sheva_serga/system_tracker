﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SystemTracker.API.Services.ClientStore;
using SystemTracker.Common.Models;

namespace SystemTracker.API.Controllers
{
    [Route("api/workstations"), ApiController]
    public class WorkstationsController : ControllerBase
    {
        private readonly IClientStoreService _storeService;

        public WorkstationsController(IClientStoreService storeService)
        {
            _storeService = storeService;
        }

        [Route("all")]
        public List<ConnectedWorkstationInfo> GetConnectedWorkstations() => 
            _storeService.GetAllWorkstations();
    }
}
