﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SystemTracker.API.Services.ClientStore;
using SystemTracker.API.Services.SignalR;
using SystemTracker.Client.Security;
using SystemTracker.Common.Security;

namespace SystemTracker.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        
        public void ConfigureServices(IServiceCollection services)
        {
            // Security config
            EncriptionOptions securityOptions = new EncriptionOptions()
            {
                Key = Configuration.GetValue<String>("SecurityKey")
            };

            services.AddSingleton(securityOptions);
            services.AddSingleton<IEncriptionService, AES256EncriptionService>();
            services.AddSingleton<ObjcetEncriptionService>();

            services.AddSingleton<IClientStoreService, InMemoryClientStoreService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddCors(options => options.AddPolicy("CorsPolicy", builder => 
                builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin()
                    .AllowCredentials()
                )
            );

            services.AddSignalR();
                
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("CorsPolicy");

            app.UseSignalR(a => a.MapHub<SystemTrackingHub>("/sessionhub"));
            app.UseHttpsRedirection();
            app.UseMvc();
            
        }
    }
}
