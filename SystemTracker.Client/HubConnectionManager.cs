﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading;
using System.Threading.Tasks;
using SystemTracker.Client.Security;
using SystemTracker.Client.WindowsInternals;
using SystemTracker.Common.Models;
using SystemTracker.Common.Security;

namespace SystemTracker.Client
{
    internal class SystemHubListener
    {
        private HubConnection _connection;

        private ObjcetEncriptionService _encriptionService;
        private InternalSessionManager _sessionMannager;
        private CancellationTokenSource _reconnectCancellation;

        private Task _asyncConnection;

        public SystemHubListener(String urlString, InternalSessionManager sessionTracker, ObjcetEncriptionService encriptionService)
        {
            _sessionMannager = sessionTracker;
            _encriptionService = encriptionService; 

            _connection = new HubConnectionBuilder()
                .WithUrl(urlString)
                .Build();

            _connection.On<Int32>("LogoffSession", OnSessionLogoffEventhHandler);

            _reconnectCancellation = new CancellationTokenSource();

            _connection.Closed += (e) => OnConnectionClosedEventHandler(e, _reconnectCancellation.Token);
        }
        
        private async Task TryConnectAsync(CancellationToken token)
        {
            Boolean isConnected = false;

            while (!isConnected)
            {
                try
                {
                    await _connection.StartAsync(token);

                    isConnected = true;

                    OnSessionChangedEventHandler(this, new WorkstationSessionsChengedEventArgs(
                            _sessionMannager.GetCurrentWorkstationInfo())
                        );
                }
                catch { }

                if (!isConnected) await Task.Delay(10000, token);

                token.ThrowIfCancellationRequested();
            }
        }

        private void OnSessionLogoffEventhHandler(Int32 sessionId)
        {
            if (_connection.State == HubConnectionState.Connected)
                _sessionMannager.LogoffSessionOnCurrentMachine(sessionId);
        }

        private Task OnConnectionClosedEventHandler(Exception exception, CancellationToken token)
        {
            if (exception != null && !token.IsCancellationRequested)
                _asyncConnection = TryConnectAsync(token);

            return Task.CompletedTask;
        }

        public void OnSessionChangedEventHandler(object sender, WorkstationSessionsChengedEventArgs e)
        {
            if (_connection?.State == HubConnectionState.Connected)
                _connection?.InvokeAsync("SendWorkstationInfo", new EncryptedString() {
                        Data = _encriptionService.Encrypt(e.WorkstationInfo)
                    })
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult();
        }

        public void Start()
        {
            _asyncConnection = TryConnectAsync(_reconnectCancellation.Token);
        }

        public Task StartAsync()
        {
            Start();

            return Task.CompletedTask;
        }

        public void Stop()
        {
            if (_connection.State == HubConnectionState.Connected)
            {
                _connection.StopAsync()
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult();
            }
            else
            {
                _reconnectCancellation.Cancel(false);
            }
        }

        public async Task StopAsync()
        {
            if (_connection.State == HubConnectionState.Connected)
            {
                await _connection.StopAsync()
                    .ConfigureAwait(false);
            }
            else
            {
                _reconnectCancellation.Cancel(false);
            }
        }
    }
}
