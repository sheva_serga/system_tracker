﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using SystemTracker.Common.Models;

namespace SystemTracker.Client.WindowsInternals
{
    [SecuritySafeCritical]
    public class InternalSessionManager
    {
        [DllImport("wtsapi32.dll", SetLastError = true)]
        private static extern bool WTSLogoffSession(IntPtr hServer, int SessionId, bool bWait);

        [DllImport("wtsapi32.dll")]
        private static extern IntPtr WTSOpenServer([MarshalAs(UnmanagedType.LPStr)] String pServerName);

        [DllImport("wtsapi32.dll")]
        private static extern void WTSCloseServer(IntPtr hServer);

        [DllImport("wtsapi32.dll")]
        private static extern Int32 WTSEnumerateSessions(IntPtr hServer, [MarshalAs(UnmanagedType.U4)] Int32 Reserved, [MarshalAs(UnmanagedType.U4)] Int32 Version, ref IntPtr ppSessionInfo, [MarshalAs(UnmanagedType.U4)] ref Int32 pCount);

        [DllImport("wtsapi32.dll")]
        private static extern void WTSFreeMemory(IntPtr pMemory);

        [DllImport("wtsapi32.dll")]
        private static extern bool WTSQuerySessionInformation(System.IntPtr hServer, int sessionId, WTS_INFO_CLASS wtsInfoClass, out System.IntPtr ppBuffer, out uint pBytesReturned);

        [StructLayout(LayoutKind.Sequential)]
        private struct WTS_SESSION_INFO
        {
            public Int32 SessionID;
            [MarshalAs(UnmanagedType.LPStr)]
            public String pWinStationName;
            public WTS_CONNECTSTATE_CLASS State;
        }

        private enum WTS_INFO_CLASS
        {
            WTSInitialProgram,
            WTSApplicationName,
            WTSWorkingDirectory,
            WTSOEMId,
            WTSSessionId,
            WTSUserName,
            WTSWinStationName,
            WTSDomainName,
            WTSConnectState,
            WTSClientBuildNumber,
            WTSClientName,
            WTSClientDirectory,
            WTSClientProductId,
            WTSClientHardwareId,
            WTSClientAddress,
            WTSClientDisplay,
            WTSClientProtocolType
        }

        private enum WTS_CONNECTSTATE_CLASS
        {
            WTSActive,
            WTSConnected,
            WTSConnectQuery,
            WTSShadow,
            WTSDisconnected,
            WTSIdle,
            WTSListen,
            WTSReset,
            WTSDown,
            WTSInit
        }

        public String GetCurrentMachineName() => Environment.MachineName;

        public WorkstationInfo GetCurrentWorkstationInfo()
        {
            Int32 sessionCount = 0;
            Int32 dataSize = Marshal.SizeOf(typeof(WTS_SESSION_INFO));

            WorkstationInfo info = new WorkstationInfo(); 
            IntPtr serverHandle = WTSOpenServer(info.MachineName = GetCurrentMachineName());

            try
            {
                IntPtr sessionInfoPtr = IntPtr.Zero, userPtr = IntPtr.Zero, domainPtr = IntPtr.Zero;

                Int32 retVal = WTSEnumerateSessions(serverHandle, 0, 1, ref sessionInfoPtr, ref sessionCount);

                Int32 currentSession = (int)sessionInfoPtr;
                uint bytes = 0;

                if (retVal != 0)
                {
                    info.Sessions = new List<SessionInfo>();

                    for (int i = 0; i < sessionCount; i++)
                    {
                        WTS_SESSION_INFO si = (WTS_SESSION_INFO)Marshal.PtrToStructure((IntPtr)currentSession, typeof(WTS_SESSION_INFO));
                        currentSession += dataSize;

                        WTSQuerySessionInformation(serverHandle, si.SessionID, WTS_INFO_CLASS.WTSUserName, out userPtr, out bytes);
                        WTSQuerySessionInformation(serverHandle, si.SessionID, WTS_INFO_CLASS.WTSDomainName, out domainPtr, out bytes);

                        String domain = Marshal.PtrToStringAnsi(domainPtr);
                        String userName = Marshal.PtrToStringAnsi(userPtr);

                        if(!String.IsNullOrEmpty(domain) && !String.IsNullOrEmpty(userName))
                        {
                            info.Sessions.Add(
                                new SessionInfo
                                {
                                    SessionId = si.SessionID,
                                    Domain = domain,
                                    UserName = userName,
                                    State = si.State.ToString()
                                }
                            );
                        }

                        WTSFreeMemory(userPtr);
                        WTSFreeMemory(domainPtr);
                    }

                    WTSFreeMemory(sessionInfoPtr);
                }
            }
            finally
            {
                WTSCloseServer(serverHandle);
            }

            return info;
        }

        public Boolean LogoffSessionOnCurrentMachine(Int32 sessionId)
        {
            IntPtr serverHandle = WTSOpenServer(GetCurrentMachineName());

            try
            {
                return WTSLogoffSession(serverHandle, sessionId, true);
            }
            finally
            {
                WTSCloseServer(serverHandle);
            }
        }
    }
}
