﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using SystemTracker.Common.Models;
using Timer = System.Timers.Timer;

namespace SystemTracker.Client.WindowsInternals
{
    public class WorkstationSessionsChengedEventArgs : EventArgs
    {
        public WorkstationInfo WorkstationInfo { get; set; }

        public WorkstationSessionsChengedEventArgs(WorkstationInfo info)
        {
            WorkstationInfo = info; 
        }

        public WorkstationSessionsChengedEventArgs() { }
    }

    public class WorkstationSessionTracker
    {
        private Timer _timer;
        private String _mashineName;

        private HashSet<SessionInfo> _sessions;

        private readonly Object _refLock = new Object();
        private readonly InternalSessionManager _internalSessionManager;

        public Boolean IsStarted => _timer != null;

        public InternalSessionManager SessionManager => _internalSessionManager;
        public WorkstationInfo WorkstationInfo => new WorkstationInfo() { MachineName = _mashineName, Sessions = _sessions.ToList() };
        
        public WorkstationSessionTracker(InternalSessionManager internalSessionManager)
        {
            _internalSessionManager = internalSessionManager;
        }

        private void TimerIterationHandler(object sender, ElapsedEventArgs e)
        {
            WorkstationInfo info = _internalSessionManager.GetCurrentWorkstationInfo();

            HashSet<SessionInfo> newSessions = new HashSet<SessionInfo>(info.Sessions);

            if (!String.Equals(info.MachineName, _mashineName) || !_sessions.SetEquals(newSessions))
            {
                Interlocked.Exchange(ref _sessions, newSessions);
                Interlocked.Exchange(ref _mashineName, info.MachineName);

                OnSessionListChanged?.Invoke(this, new WorkstationSessionsChengedEventArgs(info));
            }
        }

        public void Start()
        {
            lock (_refLock)
            {
                if (_timer == null)
                {
                    _mashineName = _internalSessionManager.GetCurrentMachineName();
                    _sessions = new HashSet<SessionInfo>();

                    _timer = new Timer(1000);
                    _timer.Elapsed += TimerIterationHandler;
                    _timer.Start();
                }
            }
        }
        public void Stop()
        {
            lock (_refLock)
            {
                if (_timer != null)
                {
                    _timer.Stop();
                    _timer.Elapsed -= TimerIterationHandler;
                    
                    _timer.Dispose();

                    _timer = null;
                }
            }
        }

        internal event EventHandler<WorkstationSessionsChengedEventArgs> OnSessionListChanged;
    }
}
