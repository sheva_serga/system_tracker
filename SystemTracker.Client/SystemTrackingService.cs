﻿using Microsoft.AspNetCore.Internal;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SystemTracker.Client.WindowsInternals;
using SystemTracker.Common.Security;

namespace SystemTracker.Client
{
    public partial class SystemTrackingService : ServiceBase
    {
        private readonly SystemHubListener _listener;
        private readonly InternalSessionManager _sessionManager;

        public SystemTrackingService(String signalRUrl, InternalSessionManager sessionManager, ObjcetEncriptionService encriptionService)
        {
            InitializeComponent();

            _sessionManager = sessionManager;

            _listener = new SystemHubListener(signalRUrl, _sessionManager, encriptionService);
        }

        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {
            _listener.OnSessionChangedEventHandler(this,
                new WorkstationSessionsChengedEventArgs(
                        _sessionManager.GetCurrentWorkstationInfo()
                    )
                );

            base.OnSessionChange(changeDescription);
        }

        protected override void OnStart(string[] args)
        {
            _listener.Start();
        }

        protected override void OnStop()
        {
            _listener.Stop();
        }


        public void Start()
        {
            _listener.Start();
        }

        public void Stop2()
        {
            _listener.Stop();
        }
    }
}
