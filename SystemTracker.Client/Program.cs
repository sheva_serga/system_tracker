﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SystemTracker.Client.WindowsInternals;
using SystemTracker.Common.Models;
using Microsoft.AspNetCore.SignalR.Protocol;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http.Connections;
using SystemTracker.Client.Security;
using SystemTracker.Common.Security;

namespace SystemTracker.Client
{
    static class Program
    {
        private static void Main()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (sender, certificate, chain, sslPolicyErrors) => true;


            // Security config
            EncriptionOptions securityOptions = new EncriptionOptions() {
                Key = ConfigurationManager.AppSettings["SecurityKey"]
            };

            IEncriptionService encriptionService = new AES256EncriptionService(securityOptions);

            var service = new SystemTrackingService(ConfigurationManager.AppSettings["SignalRUrl"], new InternalSessionManager(), new ObjcetEncriptionService(encriptionService));

            ServiceBase.Run(service);
        }
    }
}
