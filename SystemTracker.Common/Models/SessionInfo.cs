﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemTracker.Common.Models
{
    public class SessionInfo : IEquatable<SessionInfo>
    {
        public Int32 SessionId { get; set; }
        public String Domain { get; set; }
        public String UserName { get; set; }
        public String State { get; set; }

        public bool Equals(SessionInfo other)
        {
            if (other == null) return false;

            if (other == this) return true;

            if (other.GetHashCode() != GetHashCode()) return false;

            return SessionId == other.SessionId
                && String.Equals(Domain, other.Domain)
                && String.Equals(UserName, other.UserName)
                && String.Equals(State, other.State);
        }

        public override bool Equals(object obj)
        {
            SessionInfo info = obj as SessionInfo;

            return Equals(info);
        }

        public override int GetHashCode() => SessionId
            ^ Domain?.GetHashCode() ?? 0
            ^ UserName?.GetHashCode() ?? 0
            ^ State?.GetHashCode() ?? 0;
    }
}
