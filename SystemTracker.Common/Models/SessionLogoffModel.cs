﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemTracker.Common.Models
{
    public class SessionLogoffModel
    {
        public String ConnectionId { get; set; }
        public Int32 SessionId { get; set; }
    }
}
