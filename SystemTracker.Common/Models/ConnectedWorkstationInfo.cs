﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemTracker.Common.Models
{
    public class ConnectedWorkstationInfo
    {
        public String ConnectionId { get; set; }
        public WorkstationInfo Info { get; set; }

        public ConnectedWorkstationInfo() { }

        public ConnectedWorkstationInfo(String connectionId, WorkstationInfo info)
        {
            ConnectionId = connectionId;
            Info = info;
        }
    }
}
