﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemTracker.Common.Models
{
    public class WorkstationInfo
    {
        public String MachineName { get; set; }
        public List<SessionInfo> Sessions { get; set; }
    }
}
