﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using SystemTracker.Client.Security;

namespace SystemTracker.Common.Security
{
    public class ObjcetEncriptionService
    {
        private IEncriptionService _encriptionService; 

        public ObjcetEncriptionService(IEncriptionService encriptionService)
        {
            _encriptionService = encriptionService;
        }

        public String Encrypt<T>(T item)
        {
            Byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(item));

            data = _encriptionService.Encript(data);

            return Convert.ToBase64String(data);
        }

        public T Decript<T>(String item) where T : class
        {
            Byte[] data = Convert.FromBase64String(item);

            data = _encriptionService.Decript(data);

            return JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(data));
        }
    }
}
