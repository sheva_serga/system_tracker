﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemTracker.Client.Security
{
    public interface IEncriptionService
    {
        Byte[] Encript(Byte[] item);
        Byte[] Decript(Byte[] item);
    }
}
