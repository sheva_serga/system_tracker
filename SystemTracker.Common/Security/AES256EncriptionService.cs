﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SystemTracker.Client.Security;

namespace SystemTracker.Common.Security
{
    public class AES256EncriptionService : IEncriptionService
    {
        private readonly Byte[] _key;

        public AES256EncriptionService(EncriptionOptions options)
        {
            _key = Encoding.UTF8.GetBytes(options.Key);
        }

        private AesCryptoServiceProvider CreateCryptoServiceProvider() =>
             new AesCryptoServiceProvider() {
                 KeySize = 256,
                 BlockSize = 128,
                 Key = _key,
                 Padding = PaddingMode.PKCS7,
                 Mode = CipherMode.ECB,
             };

        public Byte[] Decript(Byte[] data)
        {
            using (AesCryptoServiceProvider csp = CreateCryptoServiceProvider())
            {
                ICryptoTransform decrypter = csp.CreateDecryptor();

                return decrypter.TransformFinalBlock(data, 0, data.Length);
            }
        }

        public Byte[] Encript(Byte[] data)
        {
            using (AesCryptoServiceProvider csp = CreateCryptoServiceProvider())
            {
                ICryptoTransform decrypter = csp.CreateEncryptor();

                return decrypter.TransformFinalBlock(data, 0, data.Length);
            }
        }
    }
}
